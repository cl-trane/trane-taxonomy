(in-package #:trane-taxonomy)
(eval-when (:compile-toplevel :load-toplevel :execute)
  (use-package '#:fiveam))

(def-suite :trane-taxonomy
    :description "test suite for trane-taxonomy")
(in-suite :trane-taxonomy)
