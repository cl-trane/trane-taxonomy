; -*- lisp -*-

(defpackage #:trane-taxonomy.system
  (:use #:common-lisp #:asdf))
(in-package #:trane-taxonomy.system)

(defsystem #:trane-taxonomy
  :name "trane-taxonomy"
  :description "trane-taxonomy"
  :version "0.1"
  :author ""
  :maintainer ""
  :licence "BSD sans advertising clause, see file COPYING for details"
  :depends-on (#:postmodern)
  :components
  ((:module #:src
            :components ((:file "package")
                         (:file "taxonomy" :depends-on ("package"))))))

(defsystem #:trane-taxonomy.test
  :version "0.1"
  :description "Test suite for trane-taxonomy"
  :components
  ((:module #:src
    :components ((:file "suite"))))
  :depends-on (#:trane-taxonomy #:fiveam))

(defmethod perform ((op asdf:test-op)
                    (system (eql (find-system :trane-taxonomy))))
  "Perform unit tests for trane-taxonomy"
  (asdf:operate 'asdf:load-op :trane-taxonomy.test)
  (funcall (intern (string :run!) (string :it.bese.fiveam))
           (intern (string :trane-taxonomy) (string :trane-taxonomy))))
