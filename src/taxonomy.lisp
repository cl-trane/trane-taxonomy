;;; Taxonomy Library
;;; Inspiration: http://svn.automattic.com/wordpress/trunk/wp-includes/taxonomy.php

(in-package #:trane-taxonomy)

;;; sugar
(defun make-keyword (str)
  "Convert a string or symbol to a keyword."
  (intern (string-upcase (string str)) :keyword))

;;; slugs
(defparameter +slug-chars+
  "abcdefghijklmnopqrstuvwxyz0123456789-"
  "Characters valid in slugs")

(defun valid-slug-p (str)
  "Check if string STR is a valid slug."
  (every #'(lambda (c)
             (find c +slug-chars+))
         str))

(defun slugify-char (char &aux (dc (char-downcase char)))
  "Slugify char CHAR"
  (or (when (find dc +slug-chars+)
        dc)
   
      #+sbcl
      (let* ((withindex (search "_WITH_" (char-name char)))
             (unaccented (when withindex
                           (name-char (subseq (char-name char)
                                              0 withindex)))))
        (when unaccented
          (char-downcase unaccented)))

      #\-))

(defun slugify (str)
  "Slugify whole string STR"
  (string-right-trim
   '(#\-)
   (coerce (loop
              with prevsc = #\-
              with sc = #\-
              for c across str
              do (setq sc (slugify-char c))
              unless (eq sc prevsc) collect sc
              do (setq prevsc sc))
           'string)))

;;; taxonomies
(defvar *taxonomies* nil
  "A list of defined taxonomies."
  ;; It is supposed to be just a few of them, hash table overhead
  ;; would be too big.
  )

(defclass taxonomy ()
  ((name :initarg :name :reader taxonomy-name)
   (site-dao-class :initarg :site-dao-class :reader site-dao-class)
   (item-dao-class :initarg :item-dao-class :reader item-dao-class))
  (:documentation "Base taxonomy class.

Taxonomy has a name (symbol or string, which is transformed to a
keyword by DEFTAXONOMY), and refers to DAO classes of taxonomy's
SITE and ITEM.  DAO classes are required to have an integer
primary key, accessible by reader named ID."))

(defmethod print-object ((taxonomy taxonomy) stream)
  (print-unreadable-object (taxonomy stream :type t :identity t)
    (princ (taxonomy-name taxonomy) stream)))

(defun taxonomy-db-name (taxonomy)
  "Return name of taxonomy for database (lowercased string).

Applicable to taxonomy objects, symbols and strings."
  (string-downcase (string (if (or (stringp taxonomy)
                                   (symbolp taxonomy))
                               taxonomy
                               (taxonomy-name taxonomy)))))

(defclass valued-taxonomy (taxonomy)
  ((value-encoder-function :initarg :encoder :reader value-encoder-function :initform #'identity)
   (value-decoder-function :initarg :decoder :reader value-decoder-function :initform #'identity))
  (:documentation "Taxonomy where items applied to terms may have values.

Defines encoder and decoder function slots, which are responsible
for translating value from a Lisp value to DB-safe string.  By
default it is identity function, which means values need to be
strings."))

#| FIXME:base64/cl-store/flex
(defclass cl-store-valued-taxonomy (valued-taxonomy)
  ()
  (:default-initargs :encoder #'store-to-base64 :decoder #'restore-from-base64)
  (:documentation "Valued taxonomy that by default encodes/decodes almost any Lisp object with CL-STORE as BASE64 string."))
|#

(defun encode-value (taxonomy value)
  "Encode VALUE for database, as TAXONOMY specifies.

TAXONOMY should be a VALUED-TAXONOMY instance, and VALUE should
be any value supported by the taxonomy.  When VALUE is NIL, it is
encoded as database NULL."
  (if value
      (funcall (value-encoder-function taxonomy)
               value)
      :null))

(defun decode-value (taxonomy value)
  "Decode VALUE from database, as TAXONOMY specifies.

TAXONOMY should be a VALUED-TAXONOMY instance."
  (unless (eq :null value)
    (funcall (value-decoder-function taxonomy) value)))

(defmacro deftaxonomy (name (&optional (class 'taxonomy)) &rest args)
  "Defines taxonomy named NAME, with class CLASS and initargs ARGS, and remembers it in *TAXONOMIES*.

NAME is symbol or string, which will be transformed to a keyword anyway."
  `(setf *taxonomies*
         (cons (make-instance ',class
                              :name ,(make-keyword name)
                              ,@args)
               (delete ,(make-keyword name) *taxonomies*
                       :key #'taxonomy-name))))

(defun ensure-taxonomy (taxonomy)
  "If TAXONOMY is a taxonomy object, return it, otherwise find and return taxonomy named TAXONOMY."
  (if (subtypep (class-of taxonomy) (find-class 'taxonomy))
      taxonomy
      (find (make-keyword taxonomy) *taxonomies*
            :key #'taxonomy-name)))

(defclass term ()
  ((term-id :col-type integer :reader id)
   (term-text :col-type varchar :initarg :text :accessor term-text :documentation "Full text of term")
   (term-slug :col-type varchar :accessor slug :documentation "URL-friendly version of term text, initially chosen by database")
   (site-id :col-type integer :reader term-site-id :initarg :site-id :documentation "ID of term's site")
   (term-taxonomy :col-type (varchar 32) :reader term-taxonomy-name :initarg :taxonomy-name :documentation "Name of term's taxonomy")
   (term-description :col-type text :accessor description :initarg :description :documentation "Textual description of a term"))
  (:metaclass dao-class)
  (:keys term-id)
  (:documentation "Class for a term associated with taxonomy and a site."))

(defmethod print-object ((term term) stream)
  (print-unreadable-object (term stream :type t :identity t)
    (princ (slug term) stream)))

(defun taxonomy (term)
  "TERM's taxonomy object"
  (ensure-taxonomy (term-taxonomy-name term)))

(defun term-site (term)
  "TERM's site object, if TERM is associated with a SITE."
  (let ((id (term-site-id term)))
    (when id
      (get-dao (site-dao-class (taxonomy term)) id))))

(defun ensure-term (taxonomy site &key text slug create-p)
  "Find or create term in taxonomy TAXONOMY for site SITE.

TEXT is a full text of term; if TEXT is given, CREATE-P is
non-NIL and term is not found, new term is inserted into
database.

If SLUG is given instead of TEXT, only search is possible, not
creation."
  (assert (and (or text slug)
               (not (and text slug)))
          ()
          "Either TEXT or SLUG should be given, but not both.")

  (first
   (if text
       (if create-p
           (query-dao 'term (:select '* :from (:ensure-term text (slugify text) (id site) (taxonomy-db-name taxonomy))))
           (select-dao 'term (:and (:= 'term-text text)
                                   (:= 'site-id (id site))
                                   (:= 'term-taxonomy (taxonomy-db-name taxonomy)))))
       (progn
         (when create-p
           (warn "CREATE-P is meaningful only with TEXT defined."))
         (select-dao 'term (:and (:= 'term-slug slug)
                                 (:= 'site-id (id site))
                                 (:= 'term-taxonomy (taxonomy-db-name taxonomy))))))))

(defun new-term (taxonomy site text &optional parent)
  "Create new term in SITE for TAXONOMY, with full name TEXT."
  (first (query-dao 'term
                    (:select '* :from
                             (:new-term text (slugify text) (id site) (taxonomy-db-name taxonomy) (if parent (slug parent) :null))))))

(defun find-terms-where-clause (&key taxonomy site text slug
                                &aux (query ()))
  (when taxonomy
    (push (list := 'term-taxonomy (taxonomy-db-name taxonomy))
          query))

  (when site
    (push (list := 'site-id (id site))
          query))

  (when text
    (push (list := 'term-text text) query))

  (when slug
    (push (list := 'term-slug slug) query))

  (if (> (length query) 1)
      (push :and query)
      (setf query (first query)))

  query)

(defun find-terms (&key taxonomy site text slug)
  "Find list of terms satisfying given keywords."
  (query-dao 'term
             (sql-compile `(:order-by (select '* :from 'term
                                              :where ,(find-terms-where-clause :taxonomy taxonomy
                                                                               :site site
                                                                               :text text
                                                                               :slug slug))
                                      'term-text))))

(defun apply-term (item term &optional value)
  "Apply TERM to ITEM, optionally setting its value to VALUE."
  (query (:insert-into 'item-term :set
                       'item-id (id item)
                       'term-id (id term)
                       'item-term-value (encode-value (taxonomy term) value))))

(defun term-value (item term
                   &aux (encoded (query (:select 'item-term-value :from 'item-term
                                                 :where (:and (:= 'item-id (id item))
                                                              (:= 'term-id (id term))))
                                        :single)))
  "Returns value that association of ITEM and TERM is set to.

As a second value returns T if an association was found at all,
NIL otherwise.  This makes it possible to tell between an
association with a NIL value and no association at all."
  (if encoded
      (values (decode-value (taxonomy term) encoded) t)
      (values nil nil)))

(defun (setf term-value) (new-value item term)
  "Set new value for association of ITEM and TERM.

New association between ITEM and TERM is established if it was
not present before."
  (query (:select (:set-item-term-value (id item) (id term)
                                        (encode-value  (taxonomy term) new-value)))
         :none))

(defun unbind-term (item term)
  "Deletes association between ITEM and TERM."
  (query (:delete-from 'item-term :where
                       (:and (:= 'item-id (id item))
                             (:= 'term-id (id term))))))

(defun item-terms (item &optional taxonomy)
  "List TERMs associated with ITEM in given TAXONOMY.

If TAXONOMY is not given, returns terms in all taxonomies."
  (query-dao 'term
             (sql-compile
              `(:select 'term.* :from 'term 'item-term :where
                        (:and ,@(when taxonomy
                                      (list (list :=
                                                  ''term.term-taxonomy
                                                  (taxonomy-db-name taxonomy))))
                              (:= 'item-term.item-id ,(id item))
                              (:= 'term.term-id 'item-term.term-id))))))

(defun term-item-ids (term)
  "IDs of items associated with given TERM."
  (query (:select 'item-id :from 'item-term :where (:= 'term-id (id term)))
         :column))

(defun term-items (term &aux (class (item-dao-class (taxonomy (if (integerp term) ; FIXME
                                                                  (get-dao 'term term)
                                                                  term)))) )
  "Items associated with given TERM."
  (mapcar #'(lambda (id)
              (get-dao class id))
          (term-item-ids term)))
