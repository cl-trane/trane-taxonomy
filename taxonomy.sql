-- -*- Mode: SQL; SQL-Product: Postgres; -*-

-- Terms, each associated with a taxonomy and a site
-- Taxonomy is defined on Lisp side
CREATE TABLE term (
    term_id SERIAL NOT NULL PRIMARY KEY,
    term_text VARCHAR(255) NOT NULL,
    term_slug VARCHAR(255) NOT NULL CHECK (term_slug SIMILAR TO '[a-z0-9-]+'),
    site_id INT NULL, -- REFERENCES site(site_id)
    term_taxonomy VARCHAR(32) NOT NULL DEFAULT '',
    term_description TEXT NULL
);
CREATE UNIQUE INDEX term_text_site_taxonomy_unique
                 ON term(term_text, site_id, term_taxonomy);
CREATE UNIQUE INDEX term_slug_site_taxonomy_unique
                 ON term(term_slug, site_id, term_taxonomy);

-- item/term many-to-many
CREATE TABLE item_term (
    item_id INT NOT NULL, -- REFERENCES item(item_id) ON DELETE CASCADE
    term_id INT NOT NULL REFERENCES term(term_id) ON DELETE CASCADE,
    item_term_value BLOB NULL,
    PRIMARY KEY (item_id, term_id)
);

-- Create new term.  Signals error if term exists.
-- FIXME: count existing slugs starting with new slug
CREATE OR REPLACE FUNCTION new_term(
    the_text VARCHAR, the_slug VARCHAR, the_site_id INTEGER, the_taxonomy VARCHAR)
RETURNS term
AS $body$
DECLARE
    res term;
    i INTEGER;
BEGIN
    BEGIN
        INSERT INTO term(term_text, term_slug, site_id, term_taxonomy)
             VALUES (the_text, the_slug, the_site_id, the_taxonomy);
    EXCEPTION
    WHEN unique_violation THEN
        SELECT count(*) FROM term WHERE term_slug LIKE the_slug||'%' INTO i;
        LOOP
            i = i+1;
            IF i > 999 THEN RAISE EXCEPTION 'Afraid of infinite loop.';
            END IF;
            BEGIN
                INSERT INTO term(term_text, term_slug, site_id, term_taxonomy)
                     VALUES (the_text, the_slug||'-'||i, the_site_id, the_taxonomy);
            EXCEPTION WHEN unique_violation THEN
                IF POSITION('_slug_' IN SQLERRM) <> 0 THEN CONTINUE; END IF;
                RAISE EXCEPTION 'Duplicate term.  How to re-raise from PL/PgSQL?';
            END;
            EXIT;               -- Exit loop when no unique_violation.
        END LOOP;
    END;
    SELECT INTO res * FROM term WHERE term_id=CURRVAL('term_term_id_seq');
    RETURN res;
END;
$body$ LANGUAGE plpgsql;

-- Selects or inserts term named `the_text' for site `the_site_id' in
-- taxonomy `the_taxonomy'.  Returns found/new term.  No support for
-- description or parent for new term, this function is intended to
-- work with simple terms only (e.g. tags).
CREATE OR REPLACE FUNCTION ensure_term(
    the_text VARCHAR,
    suggested_slug VARCHAR,
    the_site_id INTEGER,        -- CUSTOMIZE? function with the_site
                                -- VARCHAR which selects id by slug
                                -- and calls this one
    the_taxonomy VARCHAR)
RETURNS term
AS $body$
DECLARE
    res term;
BEGIN
    SELECT INTO res * FROM term
          WHERE term_text=the_text
            AND site_id=the_site_id
            AND term_taxonomy=the_taxonomy;
    IF NOT FOUND
    THEN res = new_term(the_text, suggested_slug, the_site_id, the_taxonomy);
    END IF;
    RETURN res;
END;
$body$ LANGUAGE plpgsql;

-- Sets term value for item (UPDATE or INSERT if needed).  When value
-- is NULL, just ensures that an association is established.
CREATE OR REPLACE FUNCTION set_item_term_value(
       the_item_id INTEGER,
       the_term_id INTEGER,
       the_new_value TEXT)
RETURNS VOID
AS $body$
BEGIN
    UPDATE item_term
           SET item_term_value=the_new_value
           WHERE item_id=the_item_id AND term_id=the_term_id;
    IF NOT FOUND THEN
        INSERT INTO item_term(item_id, term_id, item_term_value)
            VALUES(the_item_id, the_term_id, the_new_value);
    END IF;
END;
$body$ LANGUAGE PLPGSQL;
